#include "pch.hh"

#include "Loader.hh"

#include "Map.hh"

#include <iomanip>

namespace logic {

void Loader::loadAll() {
    mapMgr.load("assets/logic/maps.yaml");
    for (auto &p : mapMgr.GetObjects()) {
        p.second->loadCells(p.second->file());
    }
}

}
