#include "pch.hh"

#include "Map.hh"
#include "static/Terrain.hh"

#include <fstream>

namespace logic {

MapMgr mapMgr;

BEGIN_LOAD_FIELD(Map)
    FIELD_STR(file, file_);
END_LOAD_FIELD

void Map::loadCells(const std::string &filename) {
    std::ifstream f(filename, std::ios::in);
    if (!f.is_open()) return;
    width_ = 0;
    height_ = 0;
    while (!f.eof()) {
        std::string line;
        std::getline(f, line);
        auto w = static_cast<int32_t>(line.length() / 2);
        if (width_ == 0)
            width_ = w;
        else if (w > width_) w = width_;
        auto cur_size = static_cast<int32_t>(cellData_.size());
        cellData_.resize(cur_size + width_);
        for (int32_t i = 0; i < w; ++i) {
            auto &c = cellData_[cur_size + i];
            c.ground = stdata::terrainMgr.getObjectBySign(line[i*2]);
            // get building from (line[i*2+1]);
        }
        ++height_;
    }
    cells_.resize(width_ * height_);
    for (int32_t i = 0; i < height_; ++i)
        cells_[i] = &cellData_[width_ * i];
}

}
