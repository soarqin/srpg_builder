#pragma once

#include "common/Base.hh"
#include "common/Structs.hh"

#include "static/Movable.hh"

namespace logic {

class Unit: public common::UniqueBase {
public:
private:
    common::Pos pos_;
    const stdata::Movable *type_;
};

}
