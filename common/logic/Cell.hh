#pragma once

#include "common/Base.hh"

#include "static/Fixed.hh"
#include "static/Terrain.hh"

#include "Unit.hh"

namespace logic {

class Cell: public common::Base {
public:
    BEGIN_LOAD_FIELD_INLINE(Base)
        FIELD_INT(building, building_);
        FIELD_MEMP(unit, unit.get());
    END_LOAD_FIELD

public:
    // ground type
    std::shared_ptr<stdata::Terrain> ground;
    // building type
    std::shared_ptr<stdata::Fixed> building;
    // unit
    std::shared_ptr<Unit> unit;

private:
    int32_t building_;
};

}
