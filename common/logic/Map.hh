#pragma once

#include "common/Base.hh"
#include "Cell.hh"

namespace logic {

class Map: public common::UniqueBase {
public:
    DECL_LOAD_FIELD(UniqueBase);
    void loadCells(const std::string &filename);

    inline Cell *cell(int32_t x, int32_t y) {
        if (x < 0 || y < 0 || x >= width_ || y >= height_) return nullptr;
        return cells_[y] + x;
    }

    inline const std::string &file() { return file_; }

private:
    // map size
    int32_t width_;
    int32_t height_;
    // map file path
    std::string file_;
    // unit data
    std::vector<Cell> cellData_;
    std::vector<Cell*> cells_;
};

class MapMgr : public common::IDBaseMgr<Map> {
public:
};

extern MapMgr mapMgr;

}
