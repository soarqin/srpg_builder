#pragma once

#include "Fixed.hh"
#include "common/Structs.hh"

namespace stdata {

class Terrain: public Fixed {
public:
    // steps calculated for each moving type
    std::vector<int32_t> stepFactor;
    // ground texture
    common::TexturePos texPos;

    DECL_LOAD_FIELD(Fixed);
};

class TerrainMgr: public common::IDBaseMgr<Terrain> {
public:
    void add(std::shared_ptr<Terrain> data) override {
        signedObjs_[data->sign] = data;
        common::IDBaseMgr<Terrain>::add(std::move(data));
    }

    void remove(int32_t uid) override {
        auto ite = objs_.find(uid);
        if (ite == objs_.end()) return;
        signedObjs_.erase(ite->second->sign);
        objs_.erase(ite);
    }

    std::shared_ptr<Terrain> getObjectBySign(char sign) {
        auto ite = signedObjs_.find(sign);
        if (ite == signedObjs_.end()) return nullptr;
        return ite->second;
    }
private:
    std::map<char, std::shared_ptr<Terrain>> signedObjs_;
};

extern TerrainMgr terrainMgr;

}
