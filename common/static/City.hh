#pragma once

#include "Fixed.hh"
#include "Force.hh"

namespace stdata {

class City: public Fixed {
public:
    // maximum value for each properties
    std::vector<uint32_t> propMax;
    // initial force
    const force *initialForce;
};

}
