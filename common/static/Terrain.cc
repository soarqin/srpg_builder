#include "pch.hh"

#include "Terrain.hh"

namespace stdata {

TerrainMgr terrainMgr;

BEGIN_LOAD_FIELD(Terrain)
    FIELD_INT_VEC(steps, stepFactor);
    FIELD_MEM(tex_pos, texPos);
END_LOAD_FIELD

}
