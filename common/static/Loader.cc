#include "pch.hh"

#include "Loader.hh"

#include "Texture.hh"
#include "Terrain.hh"
#include "City.hh"
#include "Force.hh"

namespace stdata {

void Loader::loadAll() {
    textureMgr.load("assets/load/texture.yaml");
    terrainMgr.load("assets/load/terrain.yaml");
}

}
