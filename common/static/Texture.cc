#include "pch.hh"

#include "Texture.hh"

namespace stdata {

TextureMgr textureMgr;

BEGIN_LOAD_FIELD(Texture)
    FIELD_STR(path, filePath_);
END_LOAD_FIELD

BEGIN_LOAD_FIELD(TextureList)
    FIELD_MEMS_VEC(files, textures_, Texture);
END_LOAD_FIELD

bool TextureList::loaded() {
    for (auto &p: textures_) {
        textureMap_[p->uid()] = p;
    }
    return true;
}

void TextureMgr::loaded() {
    forceTex_ = get("force");
    terrainTex_ = get("terrain");
    fixedTex_ = get("fixed");
    movableTex_ = get("movable");
}

}
