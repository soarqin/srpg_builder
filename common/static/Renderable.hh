#pragma once

#include "common/Base.hh"

namespace stdata {

class Renderable: public common::UniqueBase {
public:
    // char tag for map define
    char sign;

protected:
    BEGIN_LOAD_FIELD_INLINE(common::UniqueBase)
        FIELD_CHAR(sign, sign);
    END_LOAD_FIELD
};

}
