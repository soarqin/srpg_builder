#pragma once

#include "common/Structs.hh"
#include "Renderable.hh"

namespace stdata {

class Movable: public Renderable {
private:
    common::Shape *shape_;
};

}
