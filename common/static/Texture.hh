#pragma once

#include "common/Base.hh"

namespace stdata {

class Texture: public common::UniqueBase {
public:
    inline const std::string &filePath() const { return filePath_; }

    DECL_LOAD_FIELD(UniqueBase);

private:
    std::string filePath_;
};

class TextureList: public common::UniqueBase {
public:
    DECL_LOAD_FIELD(UniqueBase);

    bool loaded() override;

private:
    std::vector<std::shared_ptr<Texture>> textures_;
    std::map<int32_t, std::shared_ptr<Texture>> textureMap_;
};

class TextureMgr: public common::NameBaseMgr<TextureList> {
public:
    void loaded() override;

private:
    std::shared_ptr<TextureList> forceTex_;
    std::shared_ptr<TextureList> terrainTex_;
    std::shared_ptr<TextureList> fixedTex_;
    std::shared_ptr<TextureList> movableTex_;
};

extern TextureMgr textureMgr;

}
