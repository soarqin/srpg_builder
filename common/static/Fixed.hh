#pragma once

#include "common/Structs.hh"

#include "Renderable.hh"

namespace stdata {

class Fixed: public Renderable {
public:
    DECL_LOAD_FIELD(Renderable);
private:
    common::Shape *shape_;
};

}
