#include "pch.hh"

#include "logic/Map.hh"
#include "MapLayer.hh"

namespace render {

void MapLayer::init() {
}

void MapLayer::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    states.transform *= getTransform();
    for (auto &p: draws_) {
        states.texture = &p.second;
        target.draw(p.first, states);
    }
}

}
