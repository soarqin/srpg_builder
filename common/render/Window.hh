#pragma once

#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

namespace render {

struct window_impl;

class Window {
public:
    Window(const std::string& name, uint32_t w, uint32_t h, bool fullscreen);
    virtual ~Window();

    void run();
    virtual void init() {}
    virtual void finish() {}
    virtual bool handle(const sf::Event&) { return false; }

protected:
    std::unique_ptr<sf::RenderWindow> window_;
    std::unique_ptr<tgui::Gui> gui_;
    std::unique_ptr<tgui::Theme> theme_;
};

}
