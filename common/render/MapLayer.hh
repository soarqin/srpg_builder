#pragma once

#include <SFML/Graphics.hpp>

namespace logic {
class Map;
}

namespace render {

class MapLayer: public sf::Drawable, public sf::Transformable  {
public:
    inline explicit MapLayer(std::shared_ptr<logic::Map> &data): data_(data) {}
    void init();

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

private:
    std::shared_ptr<logic::Map> data_;
    std::vector<std::pair<sf::VertexArray, sf::Texture>> draws_;
};

}
