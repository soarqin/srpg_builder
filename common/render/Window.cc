#include "pch.hh"

#include "Window.hh"

namespace render {

Window::Window(const std::string& name, uint32_t w, uint32_t h, bool fullscreen) {
    uint32_t style = sf::Style::Default;
    if (fullscreen) style |= sf::Style::Fullscreen;
    else style &= ~sf::Style::Fullscreen;
    sf::ContextSettings settings(24, 8, 0, 3, 2);
    window_ = std::make_unique<sf::RenderWindow>(sf::VideoMode(w, h), name, style, settings);
    window_->setVerticalSyncEnabled(true);
    gui_ = std::make_unique<tgui::Gui>(*window_);
	// theme_ = std::make_shared<tgui::Theme>("../../data/theme/Dark.png");
}

Window::~Window() {
}

void Window::run() {
    init();
    while (window_->isOpen()) {
        sf::Event event;
        while (window_->pollEvent(event)) {
            if (handle(event)) continue;
            switch (event.type) {
                case sf::Event::Closed:
                    window_->close();
                    break;
                default:
                    break;
            }
            gui_->handleEvent(event);
        }
        window_->clear();
        gui_->draw();
        window_->display();
    }
    finish();
}

}
