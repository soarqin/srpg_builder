#pragma once

#include "Base.hh"

namespace common {

struct Pos: public common::Base {
    // offset x and y
    int32_t x = 0;
    int32_t y = 0;
    BEGIN_LOAD_FIELD_INLINE(Base)
        FIELD_INT(x, x);
        FIELD_INT(y, y);
    END_LOAD_FIELD
};

struct Shape: public common::Base {
    std::vector<Pos> body;
    BEGIN_LOAD_FIELD_INLINE(Base)
        FIELD_MEM_VEC(body, body);
    END_LOAD_FIELD
};

struct TexturePos: public common::Base {
    int32_t u0 = 0;
    int32_t v0 = 0;
    int32_t u1 = 0;
    int32_t v1 = 0;
    BEGIN_LOAD_FIELD_INLINE(Base)
        FIELD_INT(u0, u0);
        FIELD_INT(v0, v0);
        FIELD_INT(u1, u1);
        FIELD_INT(v1, v1);
    END_LOAD_FIELD
};

}
