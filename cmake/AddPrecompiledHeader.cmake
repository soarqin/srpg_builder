MACRO(add_precompiled_header PrecompiledHeader PrecompiledSource SourcesVar)
  IF(MSVC)
    GET_FILENAME_COMPONENT(PrecompiledBasename ${PrecompiledHeader} NAME_WE)
    GET_FILENAME_COMPONENT(PrecompiledName ${PrecompiledHeader} NAME)
    SET(PrecompiledBinary "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CFG_INTDIR}/${PrecompiledBasename}.pch")
    SET(Sources ${${SourcesVar}})
    SET_SOURCE_FILES_PROPERTIES(${PrecompiledSource}
                                PROPERTIES COMPILE_FLAGS "/Yc\"${PrecompiledName}\" /Fp\"${PrecompiledBinary}\""
                                           OBJECT_OUTPUTS "${PrecompiledBinary}")
    SET_SOURCE_FILES_PROPERTIES(${Sources}
                                PROPERTIES COMPILE_FLAGS "/Yu\"${PrecompiledBinary}\" /FI\"${PrecompiledBinary}\" /Fp\"${PrecompiledBinary}\""
                                           OBJECT_DEPENDS "${PrecompiledBinary}")
    LIST(APPEND ${SourcesVar} ${PrecompiledSource} ${PrecompiledHeader})
    SOURCE_GROUP(pch FILES ${PrecompiledHeader} ${PrecompiledSource})
  ELSE(MSVC)
    LIST(APPEND ${SourcesVar} ${PrecompiledHeader})
    SOURCE_GROUP(pch FILES ${PrecompiledHeader})
  ENDIF(MSVC)
ENDMACRO(add_precompiled_header)
