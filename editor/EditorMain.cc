#include "pch.hh"

int main(int, char*[]) {
#if defined(_WIN32) && !defined(NDEBUG)
    AllocConsole();
    freopen("CONIN$", "r", stdin);
    freopen("CONOUT$", "w", stdout);
    freopen("CONOUT$", "w", stderr);
#endif
    return 0;
}

#if defined(_WIN32) && defined(_MSC_VER)

int WINAPI WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd) {
    return main(__argc, __argv);
}

#endif
