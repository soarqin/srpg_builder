#include "pch.hh"

#include "mainwindow.hh"

#include "static/loader.hh"
#include "logic/loader.hh"

#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>

namespace scene {

void MainWindow::init() {
    stdata::Loader::loadAll();
    logic::Loader::loadAll();

    auto tabs = tgui::Tabs::create();
    //tabs->setRenderer(theme.getRenderer("Tabs"));
    tabs->setTabHeight(30);
    tabs->setPosition(70, 40);
    tabs->add("Tab - 1");
    tabs->add("Tab - 2");
    tabs->add("Tab - 3");
    gui_->add(tabs);
}

bool MainWindow::handle(const sf::Event & evt) {
    switch (evt.type) {
    case sf::Event::Resized: break;
    default: break;
    }
    return false;
}

}
