#pragma once

#include "render/window.hh"

namespace scene {

class MainWindow: public render::Window {
public:
    using Window::Window;

    virtual void init() override;
    virtual bool handle(const sf::Event &evt) override;
};

}
